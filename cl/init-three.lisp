(defconstant +helmet-url+ "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/DamagedHelmet/glTF-Binary/DamagedHelmet.glb")
(defconstant +moon-url+ "moonlit_golf_1k.hdr")

(defconstant +three-url+ "https://cdn.skypack.dev/three@0.133.0/build/three.module.js")

(defconstant +body+ #j:document:body)

(defparameter *camera* nil)
(defparameter *obj* nil)

(defparameter *keys-pressed* (make-hash-table))

(defclass object-target ()
  ((x :initform 0.1 :accessor target-x)
   (y :initform 0.2 :accessor target-y)
   (z :initform 0.3 :accessor target-z))
  (:documentation "where a target is supposed to fly to"))

(defparameter *obj-target* (make-instance 'object-target))

(defmethod x ((obj jscl::js-object)) (jscl::oget obj "position" "x"))
(defmethod y ((obj jscl::js-object)) (jscl::oget obj "position" "y"))
(defmethod z ((obj jscl::js-object)) (jscl::oget obj "position" "z"))

(defmethod x ((obj object-target)) (target-x obj))
(defmethod y ((obj object-target)) (target-y obj))
(defmethod z ((obj object-target)) (target-z obj))

(defmethod (setf x) (new-value (obj jscl::js-object)) (jscl::oset new-value obj "position" "x"))
(defmethod (setf y) (new-value (obj jscl::js-object)) (jscl::oset new-value obj "position" "y"))
(defmethod (setf z) (new-value (obj jscl::js-object)) (jscl::oset new-value obj "position" "z"))

(defmethod (setf x) (new-value (obj object-target)) (setf (target-x obj) new-value))
(defmethod (setf y) (new-value (obj object-target)) (setf (target-y obj) new-value))
(defmethod (setf z) (new-value (obj object-target)) (setf (target-z obj) new-value))

(jscl::define-builtin /bind (fun obj)
  `(jscl::method-call ,fun "bind" ,obj))

(defmacro make-append-child (obj)
  `(/bind (jscl::oget ,obj "appendChild") ,obj))

(defun dom-element (obj)
  (jscl::oget obj "domElement"))

(defun append-child (obj child)
  (funcall (make-append-child obj) child))

(defun init-three ()
  (importing +three-url+
    (lambda (v)
      (set-window "THREE" v)
      (init-scene)
      (init-camera)
      (init-renderer)
      (init-render-func)
      (importing "https://cdn.skypack.dev/three@0.133.0/examples/jsm/controls/OrbitControls.js"
        (lambda (v)
          (set-window "OrbitControls" v)
          (make-orbit-controls)))
      (importing "https://cdn.skypack.dev/three@0.133.0/examples/jsm/loaders/GLTFLoader.js"
        (lambda (v)
          (set-window "GLTFLoader" v)))
      (importing "https://cdn.skypack.dev/three@0.133.0/examples/jsm/loaders/RGBELoader.js"
        (lambda (v)
          (set-window "RGBELoader" v)
          (add-rgbe-url-to-scene +moon-url+)))
      (add-renderer))))

(defun init-scene ()
  (defparameter *scene* (jscl::make-new #j:THREE:Scene)))

(defun init-camera ()
  (setf *camera* (jscl::js-eval "
        window.camera = new THREE.PerspectiveCamera( 45, 1.0, 0.01, 2000 );
        camera.position.set( -8.8, 2.3, 0.7 );
        window.addEventListener( 'resize', () => {
                    camera.aspect = parseInt(getComputedStyle(renderer.domElement).width) /
                                    parseInt(getComputedStyle(renderer.domElement).height) ;
                    camera.updateProjectionMatrix();
        });
        return camera;
    ")))

(defun add-keys ()
  (defun key-down (key)
    ;; (when (eq (char-code #\W) key) (incf (z *obj-target*) 1))
    ;; (when (eq (char-code #\S) key) (decf (z *obj-target*) 1))
    ;; (when (eq (char-code #\A) key) (incf (x *obj-target*) 1))
    ;; (when (eq (char-code #\D) key) (decf (x *obj-target*) 1))
    (setf (gethash key *keys-pressed*) t)
    nil)
  (defun key-up (key)
    (setf (gethash key *keys-pressed*) nil)
    nil)
  (jscl::js-eval "document.addEventListener(\"keydown\", (ev) => { if(ev.target==renderer.domElement)jscl.packages.NAGY.symbols['KEY-DOWN'].fvalue(1, ev.which) }, false)")
  (jscl::js-eval "document.addEventListener(\"keyup\",   (ev) => { if(ev.target==renderer.domElement)jscl.packages.NAGY.symbols['KEY-UP'].fvalue(1, ev.which) }, false)"))

(defun init-renderer ()
  (defparameter *renderer* (jscl::js-eval "
        window.renderer = new THREE.WebGLRenderer( { antialias: true } );
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.domElement.setAttribute('tabindex', 1);
        renderer.toneMapping = THREE.ACESFilmicToneMapping;
        renderer.toneMappingExposure = 1;
        renderer.outputEncoding = THREE.sRGBEncoding;
        window.addEventListener( 'resize', () => {
                renderer.setSize( parseInt(getComputedStyle(renderer.domElement).width),
                                  parseInt(getComputedStyle(renderer.domElement).height), false );
          });
        return renderer;
    "))
  )


(defun init-render-func ()
  (defparameter *lisp-render-func*
    (lambda (delta)
      (when *obj-target*
        (when (gethash (char-code #\W) *keys-pressed*) (incf (z *obj-target*) 0.1))
        (when (gethash (char-code #\S) *keys-pressed*) (decf (z *obj-target*) 0.1))
        (when (gethash (char-code #\A) *keys-pressed*) (incf (x *obj-target*) 0.1))
        (when (gethash (char-code #\D) *keys-pressed*) (decf (x *obj-target*) 0.1)))
      (when *obj*
        (let ((diff-x (- (x *obj-target*) (x *obj*)))
             (diff-y (- (y *obj-target*) (y *obj*)))
             (diff-z (- (z *obj-target*) (z *obj*))))
          (incf (x *obj*) (/ diff-x 15))
          (incf (y *obj*) (/ diff-y 15))
          (incf (z *obj*) (/ diff-z 15)))
        (if (and (< 1 (x *obj*))
             (< 1 (z *obj*)))
          (setf (y *obj-target*) 2)
          (setf (y *obj-target*) 0))
        ((jscl::oget *camera* "lookAt") (x *obj*) (y *obj*) (z *obj*)))
      ))
  (defparameter *render-func* (jscl::js-eval "
        //window.mixer = null;
        window.render = function(delta = 0) {
            //if(mixer)mixer.update(0.001);
            try {
               jscl.packages['NAGY'].symbols['*LISP-RENDER-FUNC*'].value( 1, 0 );
            } catch(e) { console.log('error', e); }
            renderer.render( jscl.packages['NAGY'].symbols['*SCENE*'].value,
                             jscl.packages['NAGY'].symbols['*CAMERA*'].value);
            requestAnimationFrame( render );
        }
        window.load_gltf=function(url,cb){new GLTFLoader.GLTFLoader().load(url,(gltf)=>{window.mixer=new THREE.AnimationMixer( gltf.scene);gltf.animations.forEach((clip)=>{mixer.clipAction(clip ).play()});cb(gltf)})};
        window.load_rgbe=function(url,cb){new RGBELoader.RGBELoader().load(url,(texture)=>{cb(texture)})};
    ")))

(defun make-orbit-controls ()
  (defparameter *orbit-controls*
    (let ((dom-element (jscl::oget *renderer* "domElement")))
      (jscl::make-new (jscl::oget #j:OrbitControls "OrbitControls") *camera* dom-element))))

(defun add-to-scene (obj &optional (scene *scene*))
  (when scene
    ((jscl::oget scene "add") obj)))

(defun rm-from-scene (obj &optional (scene *scene*))
  (when scene
    ((jscl::oget scene "remove") obj)))

(defun add-gltf-url-to-scene (url)
  (funcall #j:load_gltf url
    (lambda (gltf)
      (setf *obj* (jscl::oget gltf "scene"))
      (add-keys)
      (add-to-scene (jscl::oget gltf "scene")))))

(defun add-rgbe-url-to-scene (url)
  (funcall #j:load_rgbe url
    (lambda (texture)
      (setf (jscl::oget texture "mapping") #j:THREE:EquirectangularReflectionMapping)
      (setf (jscl::oget *scene* "environment") texture)
      (setf (jscl::oget *scene* "background") texture))))

(defun render () (#j:render))

(defun add-renderer ()
  (append-child +body+ (dom-element *renderer*))
  (jscl::js-eval "
        renderer.setSize( parseInt(getComputedStyle(renderer.domElement).width),
                          parseInt(getComputedStyle(renderer.domElement).height), false );
        camera.aspect = parseInt(getComputedStyle(renderer.domElement).width) /
                        parseInt(getComputedStyle(renderer.domElement).height) ;
        camera.updateProjectionMatrix();

        const resizeObserver = new ResizeObserver(entries => {
            renderer.setSize( parseInt(getComputedStyle(renderer.domElement).width),
                              parseInt(getComputedStyle(renderer.domElement).height), false );
            camera.aspect = parseInt(getComputedStyle(renderer.domElement).width) /
                            parseInt(getComputedStyle(renderer.domElement).height) ;
            camera.updateProjectionMatrix();
        });

        resizeObserver.observe(renderer.domElement);
  ")
  (render))

(init-three)

(defun add-helmet ()
  (add-gltf-url-to-scene +helmet-url+))

(defun (setf look-at) (new-value obj)
  ((jscl::oget obj "lookAt") (car new-value) (cadr new-value) (caddr new-value)))

(defun pick-folder ()
  (jscl::js-eval
    "showDirectoryPicker().then( async (v) => {
            for await(const entry of v.values()) {
                new GLTFLoader.GLTFLoader().parse( await (await entry.getFile()).arrayBuffer(), '/', function(f){
                   jscl.packages.NAGY.symbols['*SCENE*'].value.add(f.scene)
                });
            }
          })"))

import SExpressionParser from "./sexpparser.js"

function WebSocketInit(applyInstrFunc, onmsg) {
    busCallAddMatch("type=signal,interface=proglines2dbus.local")
    window.busSignalEvents.addEventListener("signal", function(evt) {
        if(evt.msg.interface != "proglines2dbus.local") {
            return
        }
        if(evt.msg.path != "/") {
            return
        }
        if(evt.msg.member != "ReadLine") {
            return
        }
        const parsed = SExpressionParser.parse(evt.msg.objects[0]);
        //console.log(parsed)
        if(Array.isArray(parsed)) {
            //console.log(parsed)
            for(const instr of parsed) {
                applyInstrFunc(instr)
            }
        }
        onmsg?.()
    });
}

function wsSend(txt) {
    busCallArgs("proglines2dbus.ws8081", "/", "proglines2dbus.local", "WriteLine", "s", [txt])
}

function wsSetPositions(num, posvec) {
    wsSend(`set ${num} :px ${posvec.x}`)
    wsSend(`set ${num} :py ${posvec.y}`)
    wsSend(`set ${num} :pz ${posvec.z}`)
}

function wsSetRotations(num, rotvec) {
    wsSend(`set ${num} :rx ${rotvec.x}`)
    wsSend(`set ${num} :ry ${rotvec.y}`)
    wsSend(`set ${num} :rz ${rotvec.z}`)
}

export { WebSocketInit, wsSetPositions, wsSetRotations, wsSend }
